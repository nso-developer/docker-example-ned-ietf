# determine our project name, either from CI_PROJECT_NAME which is normally set
# by GitLab CI or by looking at the name of our directory (that we are in)
ifneq ($(CI_PROJECT_NAME),)
PROJECT_NAME=$(CI_PROJECT_NAME)
else
PROJECT_NAME=$(shell basename $(PWD))
endif

# determine the NSO package name, which is assumed to be a sub-directory at the
# top level in the project directory. We look for src/package-meta-data.xml*
# which is then assumed to be a NSO package.
ifeq ($(PACKAGE_NAME),)
ifeq ($(shell ls */src/package-meta-data.xml* | wc -l),0)
$(error Could not determine NED package automatically. No directory found based on globa */src/package-meta-data.xml*)
else ifeq ($(shell ls */src/package-meta-data.xml* | wc -l),1)
PACKAGE_NAME=$(shell dirname $(shell dirname $(shell ls */src/package-meta-data.xml*)))
else
$(error Could not determine NED package automatically. Multiple directories found based on glob */src/package-meta-data.xml*)
endif
endif

# compute docker tag to use for the docker images we produce
ifneq ($(CI_JOB_ID),)
DOCKER_TAG=$(CI_JOB_ID)
else
DOCKER_TAG=$(shell whoami)-$(NSO_VERSION)
endif

# Path for the NSO docker images (NSO_IMAGE_PATH) is derived based on
# information we get from Gitlab CI, if available. Similarly, the path we use
# for the images we produce is also based on information from Gitlab CI.
ifneq ($(CI_REGISTRY),)
NSO_IMAGE_PATH?=$(CI_REGISTRY)/$(CI_PROJECT_NAMESPACE)/nso-docker/
IMAGE_PATH?=$(CI_REGISTRY_IMAGE)/
endif

NSO_TAG?=$(NSO_VERSION)
BUILD_IMAGE?=$(NSO_IMAGE_PATH)cisco-nso-dev:$(NSO_TAG)
BASE_IMAGE?=$(NSO_IMAGE_PATH)cisco-nso-base:$(NSO_TAG)


.PHONY: test

all:
	$(MAKE) build
	$(MAKE) test
	$(MAKE) stop

build:
	docker build --target netsim -t $(IMAGE_PATH)$(PROJECT_NAME)-netsim:$(DOCKER_TAG) --build-arg BUILD_IMAGE=$(BUILD_IMAGE) --build-arg BASE_IMAGE=$(BASE_IMAGE) --build-arg PACKAGE_NAME=$(PACKAGE_NAME) .
	docker build --target nso -t $(IMAGE_PATH)$(PROJECT_NAME)-nso:$(DOCKER_TAG) --build-arg BUILD_IMAGE=$(BUILD_IMAGE) --build-arg BASE_IMAGE=$(BASE_IMAGE) --build-arg PACKAGE_NAME=$(PACKAGE_NAME) .
	docker build --target ned -t $(IMAGE_PATH)$(PROJECT_NAME):$(DOCKER_TAG) --build-arg BUILD_IMAGE=$(BUILD_IMAGE) --build-arg BASE_IMAGE=$(BASE_IMAGE) --build-arg PACKAGE_NAME=$(PACKAGE_NAME) .

test:
	$(MAKE) stop
	$(MAKE) clean
	docker network create ci-$(DOCKER_TAG)
	docker run -td --network ci-$(DOCKER_TAG) --name $(DOCKER_TAG)-nso $(IMAGE_PATH)$(PROJECT_NAME)-nso:$(DOCKER_TAG)
	docker run -td --network ci-$(DOCKER_TAG) --name $(DOCKER_TAG)-netsim --network-alias dev1 $(IMAGE_PATH)$(PROJECT_NAME)-netsim:$(DOCKER_TAG)
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'ncs --wait-started 600'
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo "show packages" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo "show packages" | ncs_cli -u admin -g ncsadmin' | grep "oper-status up"
	@echo "Add device to NSO"
	@echo "Copy config straight, for NSO 5"
	cp test/add-device.xml add-device.xml
	@echo "On NSO 4 we remove the ned-id from the config since that's a NSO 5 thing"
	-echo $(NSO_VERSION) | grep "^4" && sed -e '/<ned-id/d' test/add-device.xml > add-device.xml
	docker cp add-device.xml $(DOCKER_TAG)-nso:/add-device.xml
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo -e "configure\nload merge /add-device.xml\ncommit\nexit" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo "show devices brief" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo "request devices device dev1 ssh fetch-host-keys" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo "request devices device dev1 sync-from" | ncs_cli -u admin -g ncsadmin'
	@echo "Configure hostname on device through NSO"
	docker cp test/device-config-hostname.xml $(DOCKER_TAG)-nso:/device-config-hostname.xml
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo -e "configure\nload merge /device-config-hostname.xml\ncommit\nexit" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo -e "show configuration devices device dev1 config sys:system hostname" | ncs_cli -u admin -g ncsadmin' | grep foobar
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo -e "request devices device dev1 sync-from" | ncs_cli -u admin -g ncsadmin'
	docker exec -t $(DOCKER_TAG)-nso bash -lc 'echo -e "show configuration devices device dev1 config sys:system hostname" | ncs_cli -u admin -g ncsadmin' | grep foobar

stop:
	-docker stop $(DOCKER_TAG)-netsim $(DOCKER_TAG)-nso

clean:
	-docker rm -f $(DOCKER_TAG)-netsim $(DOCKER_TAG)-nso
	-docker network rm ci-$(DOCKER_TAG)

push:
	docker push $(IMAGE_PATH)$(PROJECT_NAME):$(DOCKER_TAG)

tag-release:
	@echo "Setting docker tag for release"
	docker tag $(IMAGE_PATH)$(PROJECT_NAME):$(DOCKER_TAG) $(IMAGE_PATH)$(PROJECT_NAME):$(NSO_VERSION)
	docker tag $(IMAGE_PATH)$(PROJECT_NAME)-netsim:$(DOCKER_TAG) $(IMAGE_PATH)$(PROJECT_NAME)-netsim:$(NSO_VERSION)

push-release:
	docker push $(IMAGE_PATH)$(PROJECT_NAME):$(NSO_VERSION)
